# General
- `nixos-rebuild switch --flake "gitlab:michzappa/nix#<output>`
- `darwin-rebuild switch --flake "gitlab:michzappa/nix#<output>`

# Install Nixos
- read, and then run `hosts/<host>/install.sh`

# Install nix-darwin
- follow [nix-darwin](https://github.com/LnL7/nix-darwin "nix-darwin") instructions
- `darwin-rebuild switch --flake "gitlab:michzappa/nix#<output>`
- `chsh -s /run/current-system/sw/bin/fish`

# Organization
- Entry point modules for NixOS and nix-darwin are in `hosts/`, which uses `modules/`
