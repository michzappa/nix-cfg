{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
let cfg = config.mz.emacs;
in {
  options.mz.emacs = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable {
    mz.home = {
      programs = {
        emacs = {
          enable = true;
          extraPackages = (epkgs: (with epkgs; [ pdf-tools vterm ]));
        };
        fish.functions = { ec = "emacsclient -t -a '' $argv"; };
      };
    };
  };
}
