{ lib, ... }:

with lib;
let
  dirContents = dir:
    mapAttrs (file: type:
      if type == "directory" then dirContents "${dir}/${file}" else type)
    (builtins.readDir dir);

  filesInDir = dir:
    collect isString (mapAttrsRecursive (path: type: concatStringsSep "/" path)
      (dirContents dir));

  filesToImport = dir:
    map (file: "${./.}/${file}")
    (filter (file: hasSuffix ".nix" file && file != "default.nix")
      (filesInDir dir));

in { imports = filesToImport ./.; }
