{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.gnupg;
in {
  options.mz.gnupg = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable (nixosOptions {
    programs.gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      pinentryFlavor = "curses";
    };
  });
}
