{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.pipewire;
in {
  options.mz.pipewire = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable (nixosOptions {
    hardware.pulseaudio.enable = false;

    security.rtkit.enable = true;

    services = {
      pipewire = {
        enable = true;
        alsa = {
          enable = true;
          support32Bit = true;
        };
        pulse.enable = true;
      };
    };

    sound.enable = false;
  });
}
