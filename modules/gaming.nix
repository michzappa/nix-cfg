{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.gaming;
in {
  options.mz.gaming = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable (nixosOptions {
    hardware.opengl.driSupport32Bit = true;
    mz.user.packages = with pkgs; [ samba wineWowPackages.staging ];
    programs.steam.enable = true;
  });
}
