{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz; {
  options.mz.home = {
    dconf = mkOption {
      type = types.attrs;
      default = { };
    };

    file = mkOption {
      type = types.attrs;
      default = { };
    };

    packages = mkOption {
      type = types.listOf types.package;
      default = [ ];
    };

    programs = mkOption {
      type = types.attrs;
      default = { };
    };

    systemd = mkOption {
      type = types.attrs;
      default = { };
    };

    xdg-file = mkOption {
      type = types.attrs;
      default = { };
    };
  };

  config = (mkMerge [
    (systemOptions {
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        users.${config.mz.user.name} = {
          home = {
            file = mkAliasDefinitions options.mz.home.file;
            packages = mkAliasDefinitions options.mz.home.packages;
            # TODO can the darwin value be abstracted? is it anywhere in the input?
            stateVersion = if (platform == "nixos") then
              config.system.stateVersion
            else
              "22.11";
          };
          dconf = mkAliasDefinitions options.mz.home.dconf;
          programs = mkAliasDefinitions options.mz.home.programs;
          systemd = mkAliasDefinitions options.mz.home.systemd;
          xdg.configFile = mkAliasDefinitions options.mz.home.xdg-file;
        };
      };
    })

    (homeOptions {
      home = {
        file = mkAliasDefinitions options.mz.home.file;
        packages = mkAliasDefinitions options.mz.home.packages;
        stateVersion = "22.11";
      };
      programs = mkAliasDefinitions options.mz.home.programs;
      systemd = mkAliasDefinitions options.mz.home.systemd;
      xdg.configFile = mkAliasDefinitions options.mz.home.xdg-file;
    })
  ]);
}
