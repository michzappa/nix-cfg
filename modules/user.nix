{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz; {
  options.mz.user = {
    name = mkOption {
      type = types.str;
      default = "michael";
    };

    extraGroups = mkOption {
      type = types.listOf types.str;
      default = [ ];
    };

    packages = mkOption {
      type = types.listOf types.package;
      default = [ ];
    };

    password = mkOption {
      type = types.nullOr types.str;
      default = null;
    };

    shell = mkOption {
      type = types.nullOr types.shellPackage;
      default = null;
    };
  };

  config = (systemOptions {
    users.users.${config.mz.user.name} = (mkMerge [
      (nixosOptions {
        extraGroups = mkAliasDefinitions options.mz.user.extraGroups;
        isNormalUser = true;
        password = mkAliasDefinitions options.mz.user.password;
      })

      {
        name = mkAliasDefinitions options.mz.user.name;
        packages = mkAliasDefinitions options.mz.user.packages;
        shell = mkAliasDefinitions options.mz.user.shell;
      }
    ]);
  });
}
