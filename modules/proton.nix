{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
let cfg = config.mz.proton;
in {
  options.mz.proton = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable {
    mz.home = {
      packages = with pkgs; [ protonmail-bridge protonvpn-gui ];
      systemd.user.services.protonmail-bridge = {
        Install.WantedBy = [ "default.target" ];
        Service = {
          Environment = "PATH=${pkgs.gnome.gnome-keyring}/bin";
          ExecStart =
            "${pkgs.protonmail-bridge}/bin/protonmail-bridge --no-window --log-level debug";
        };
        Unit = {
          After = [ "network.target" ];
          Description = "ProtonMail Bridge";
        };
      };
    };
  };
}
