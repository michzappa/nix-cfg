{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.mullvad;
in {
  options.mz.mullvad = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable (nixosOptions {
    environment.systemPackages = with pkgs; [
      gnomeExtensions.mullvad-indicator
      mullvad-vpn
    ];

    services.mullvad-vpn.enable = true;
  });
}
