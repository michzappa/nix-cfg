{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.kmonad;
in {
  options.mz.kmonad = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };

    config = mkOption {
      type = types.str;
      default = null;
    };

    device = mkOption {
      type = types.str;
      default = null;
    };

    name = mkOption {
      type = types.str;
      default = null;
    };
  };

  config = mkIf cfg.enable (nixosOptions {
    services = {
      kmonad = {
        enable = true;
        keyboards.${cfg.name} = {
          config = cfg.config;
          device = cfg.device;
          defcfg = {
            enable = true;
            allowCommands = false;
            fallthrough = true;
          };
        };
      };
    };
  });
}
