{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.fish;
in {
  options.mz.fish = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable (mkMerge [
    {
      mz = {
        home = {
          programs.fish = {
            enable = true;
            functions = {
              calibre = "nix run ${inputs.nixpkgs.outPath}#calibre -- calibre";
              dev = "nix develop ${inputs.nixpkgs.outPath}#$argv[1]";
              dot-build = "nix build .#$argv[1]";
              gimme = "nix shell ${inputs.nixpkgs.outPath}#$argv";
              lookup = "nix search path:${inputs.nixpkgs.outPath} $argv[1]";
              nix-clear-result-dirs = ''
                nix-store --gc --print-roots |\
                  awk '{print $1}' |\
                  grep /result |\
                  tee /dev/tty |\
                  sudo xargs rm
              '';
              # the local filepath is tightly coupled, cannot be abstracted
              rebuild = "${
                  if (platform == "nixos") then
                    "nixos-rebuild --use-remote-sudo"
                  else if (platform == "darwin") then
                    "darwin-rebuild"
                  else
                    "home-manager"
                } $argv[1] --flake ~/projects/nixcfg#${config.mz.system.name}";
              rebuild-vm =
                "nixos-rebuild build-vm --flake ~/projects/nixcfg#faux";
              weather = ''
                if test (count $argv) -lt 1;
                   curl wttr.in
                else
                   curl wttr.in/$argv[1]
                end
              '';
            };
            interactiveShellInit = with pkgs; ''
              set fish_greeting
              function fish_mode_prompt; end
              function fish_prompt; end

              # disable the annoying $EDITOR keybindings by remapping to nop
              bind \ee true
              bind \ev true

              ${any-nix-shell}/bin/any-nix-shell fish --info-right | source
              ${starship}/bin/starship init fish | source
            '';
          };
          xdg-file."starship.toml".text = ''
            add_newline = false

            [character]
            success_symbol = "[➜](bold green)"

            [cmd_duration]
            min_time = 5
            show_milliseconds = true

            [directory]
            truncation_length = 0

            [package]
            disabled = true
          '';
        };
        user.shell = pkgs.fish;
      };

      programs.fish.enable = true;
    }

    (systemOptions { environment.shells = with pkgs; [ fish ]; })

    (darwinOptions {
      system.activationScripts.postActivation.text = ''
        sudo chsh -s ${pkgs.fish}/bin/fish ${config.mz.user.name}
      '';
    })
  ]);
}
