{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.gnome;
in {
  options.mz.gnome = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };

    dconf-defaults = mkOption {
      type = types.bool;
      default = true;
    };
  };

  config = mkIf cfg.enable (nixosOptions {
    environment.systemPackages = with pkgs; [
      dconf2nix
      gnome.gnome-terminal
      gnome.gnome-tweaks
      gnomeExtensions.appindicator
      wl-clipboard
      xclip
    ];

    fonts = {
      fonts = with pkgs; [
        noto-fonts
        noto-fonts-cjk
        noto-fonts-emoji
        noto-fonts-extra
      ];
    };

    mz = {
      home = with lib.hm.gvariant;
        mkIf cfg.dconf-defaults {
          dconf.settings = {
            "org/freedesktop/ibus/engine/anthy/common" = {
              conversion-segment-mode = 0;
              input-mode = 0;
              show-dict-mode = false;
              show-typing-method = true;
              typing-method = 0;
            };

            "org/gnome/desktop/background" = {
              color-shading-type = "solid";
              picture-options = "zoom";
              picture-uri =
                "file:///run/current-system/sw/share/backgrounds/gnome/adwaita-l.webp";
              picture-uri-dark =
                "file:///run/current-system/sw/share/backgrounds/gnome/adwaita-d.webp";
              primary-color = "#3071AE";
              secondary-color = "#000000";
            };

            "org/gnome/desktop/input-sources" = {
              per-window = false;
              sources = [
                (mkTuple [ "xkb" "fr+us" ])
                (mkTuple [ "xkb" "us" ])
                (mkTuple [ "xkb" "it+us" ])
                (mkTuple [ "ibus" "anthy" ])
                (mkTuple [ "ibus" "libpinyin" ])
                (mkTuple [ "ibus" "rime" ])
              ];
              xkb-options = [ "terminate:ctrl_alt_bksp" ];
            };

            "org/gnome/desktop/interface" = {
              clock-format = "12h";
              color-scheme = "prefer-dark";
              font-antialiasing = "grayscale";
              font-hinting = "slight";
              gtk-theme = "Adwaita-dark";
              show-battery-percentage = true;
            };

            "org/gnome/desktop/peripherals/touchpad" = {
              natural-scroll = false;
              two-finger-scrolling-enabled = true;
            };

            "org/gnome/desktop/screensaver" = {
              color-shading-type = "solid";
              picture-options = "zoom";
              picture-uri =
                "file:///run/current-system/sw/share/backgrounds/gnome/adwaita-l.jpg";
              picture-uri-dark =
                "file:///run/current-system/sw/share/backgrounds/gnome/adwaita-d.jpg";
              primary-color = "#3465a4";
              secondary-color = "#000000";
            };

            "org/gnome/desktop/wm/keybindings" = {
              close = [ "<Super>q" ];
              switch-applications = [ ];
              switch-applications-backward = [ ];
              switch-windows = [ "<Alt>Tab" ];
              switch-windows-backward = [ "<Shift><Alt>Tab" ];
            };

            "org/gnome/desktop/wm/preferences" = {
              button-layout = "appmenu:minimize,maximize,close";
            };

            "org/gnome/mutter" = {
              experimental-features = [ "scale-monitor-framebuffer" ];
              workspaces-only-on-primary = false;
            };

            "org/gnome/settings-daemon/plugins/media-keys" = {
              custom-keybindings = [
                "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
                "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/"
                "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2/"
              ];
              help = [ ];
              home = [ "<Super>f" ];
              www = [ "<Super>b" ];
            };

            "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" =
              {
                binding = "<Super>t";
                command = "gnome-terminal";
                name = "terminal";
              };

            "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" =
              {
                binding = "<Super>e";
                command = ''emacsclient -c -a ""'';
                name = "emacsclient";
              };

            "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom2" =
              {
                binding = "<Shift><Super>e";
                command = "emacs";
                name = "emacs";
              };

            "org/gnome/shell" = {
              disable-user-extensions = false;
              disabled-extensions = [
                "workspace-indicator@gnome-shell-extensions.gcampax.github.com"
                "windowsNavigator@gnome-shell-extensions.gcampax.github.com"
                "window-list@gnome-shell-extensions.gcampax.github.com"
                "screenshot-window-sizer@gnome-shell-extensions.gcampax.github.com"
                "drive-menu@gnome-shell-extensions.gcampax.github.com"
                "apps-menu@gnome-shell-extensions.gcampax.github.com"
                "launch-new-instance@gnome-shell-extensions.gcampax.github.com"
              ];
              enabled-extensions = [
                "appindicatorsupport@rgcjonas.gmail.com"
                "places-menu@gnome-shell-extensions.gcampax.github.com"
                "gsconnect@andyholmes.github.io"
                "mullvadindicator@pobega.github.com"
              ];
              favorite-apps = [
                "firefox.desktop"
                "org.gnome.Terminal.desktop"
                "org.gnome.Nautilus.desktop"
                "emacs.desktop"
              ];
              had-bluetooth-devices-setup = true;
              welcome-dialog-last-shown-version = "41.1";
            };

            "org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9" =
              {
                audible-bell = false;
                font = "DejaVu Sans Mono 20";
                use-system-font = false;
                visible-name = "michael";
              };

            "org/gnome/tweaks" = { show-extensions-notice = false; };
          };
        };
      user.extraGroups = [ "networkmanager" "uinput" ];
    };

    i18n = {
      inputMethod = {
        enabled = "ibus";
        # haven't been able to get rime's multi-input working, thus
        # separate IMEs for simplified and traditional pinyin
        ibus.engines = with pkgs.ibus-engines; [ anthy libpinyin rime ];
      };
    };

    programs.kdeconnect = {
      enable = true;
      package = pkgs.gnomeExtensions.gsconnect;
    };

    services = {
      xserver = {
        enable = true;
        desktopManager.gnome.enable = true;
        displayManager.gdm.enable = true;
        libinput.enable = true;
      };
    };
  });
}
