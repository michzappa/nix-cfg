{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
let cfg = config.mz.reasoning;
in {
  options.mz.reasoning = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable {
    mz.home = {
      file.".agda/defaults".text = "standard-library";
      packages = with pkgs; [
        (agda.withPackages [ agdaPackages.standard-library ])
        coq
        lean
      ];
      programs.emacs.extraPackages =
        (epkgs: (with epkgs; [ agda2-mode lean-mode proof-general ]));
    };
  };
}
