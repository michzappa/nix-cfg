{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.system;
in {
  options.mz.system = {
    default-config = mkOption {
      type = types.bool;
      default = true;
    };

    default-apps = mkOption {
      type = types.bool;
      default = true;
    };

    name = mkOption {
      type = types.str;
      default = config.networking.hostName;
    };
  };

  config = mkIf cfg.default-config (mkMerge [
    {
      nix = {
        extraOptions = ''
          experimental-features = nix-command flakes
        '';
        package = pkgs.nix;
      };

      nixpkgs.config.allowUnfree = true;
    }

    (systemOptions {
      environment.systemPackages = with pkgs; [ nixfmt ];

      nix = {
        settings = {
          auto-optimise-store = true;
          trusted-users = [ "root" "@wheel" ];
        };
      };
    })

    (darwinOptions { services.nix-daemon.enable = true; })

    (nixosOptions {
      environment.systemPackages = with pkgs; [
        busybox
        emacs
        gcc
        gnumake
        ispell
        qemu
        ripgrep
        screen
        texlive.combined.scheme-full
        tree
        unzip
        wget
      ];

      i18n.defaultLocale = "en_US.UTF-8";

      mz.user = {
        extraGroups = [ "wheel" ];
        packages = with pkgs;
          mkIf cfg.default-apps [
            calibre
            chromium
            discord
            element-desktop
            firefox-wayland
            freetube
            gramps
            gthumb
            knock
            signal-desktop
            spotify
            teams
            thunderbird
            zoom-us
          ];
      };

      networking = {
        firewall.enable = true;
        useDHCP = false;
      };

      nixpkgs.overlays = [
        (self: super: {
          knock = inputs.knock.outputs.packages.${pkgs.system}.knock;
        })
        inputs.emacs.overlay
      ];

      security.sudo.wheelNeedsPassword = false;
    })
  ]);
}
