{ config, inputs, lib, options, pkgs, platform, ... }:

with lib;
with lib.mz;
let cfg = config.mz.docker;
in {
  options.mz.docker = {
    enable = mkOption {
      type = types.bool;
      default = false;
    };
  };

  config = mkIf cfg.enable (nixosOptions {
    mz.user.extraGroups = [ "docker" ];

    virtualisation.docker.enable = true;
  });
}
