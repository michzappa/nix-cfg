{
  description = "michzappa's nixos configurations";

  inputs = {
    # packages
    emacs.url = "github:nix-community/emacs-overlay";
    kmonad.url = "github:kmonad/kmonad?dir=nix";
    knock.url = "github:BentonEdmondson/knock";

    # configuration
    darwin.url = "github:lnl7/nix-darwin/master";
    home-manager.url = "github:nix-community/home-manager";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nixpkgs.url = "github:NixOs/nixpkgs/nixos-unstable";
  };

  outputs = { self, ... }@inputs:
    with inputs;
    let
      build-lib = platform:
        nixpkgs.lib.extend (final: prev:
          home-manager.lib // {
            mz = import ./lib.nix {
              inputs = inputs;
              lib = final;
              platform = platform;
            };
          });
    in rec {
      lib = {
        darwin = build-lib "darwin";
        home = build-lib "home";
        nixos = build-lib "nixos";
      };

      darwinConfigurations = {
        amz = lib.darwin.mz.makeConfiguration "amz" "aarch64-darwin";
      };

      homeConfigurations = {
        amz-dev = lib.home.mz.makeConfiguration "amz-dev" "x86_64-linux";
      };

      nixosConfigurations = {
        faux = lib.nixos.mz.makeConfiguration "faux" "x86_64-linux";
        ordinateur = lib.nixos.mz.makeConfiguration "ordinateur" "x86_64-linux";
        rorohiko = lib.nixos.mz.makeConfiguration "rorohiko" "x86_64-linux";
      };
    };
}
