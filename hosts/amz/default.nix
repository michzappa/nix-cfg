{ config, lib, pkgs, ... }:

{
  home-manager.users."mzappa" = {
    programs.fish = {
      functions = {
        "2fa-setup" = "kinit && mwinit -o -s";
        "bastion-setup" =
          "mwinit --aea -s && sudo jamf policy -event configure-security-bastions";
        "ssh-cloud-desktop" = "ssh mzappa-clouddesk.aka.corp.amazon.com";
      };
      interactiveShellInit = ''
        fish_add_path /Users/mzappa/.toolbox/bin
        fish_add_path /opt/homebrew/bin
        fish_add_path /opt/homebrew/opt/node@14/bin
      '';
    };
  };

  mz = {
    direnv.enable = true;
    emacs.enable = true;
    fish.enable = true;
    system.name = "amz";
    user.name = "mzappa";
  };

  environment.systemPackages = with pkgs; [ ispell ];

  services = {
    skhd = {
      enable = true;
      skhdConfig = ''
        rcmd - e : emacsclient -c -a ""
        rcmd - t : open -a /Applications/iTerm.app

        rcmd - j : yabai -m window --focus west
        rcmd - k : yabai -m window --focus south
        rcmd - i : yabai -m window --focus north
        rcmd - l : yabai -m window --focus east

        shift + rcmd - j : yabai -m window --swap west
        shift + rcmd - k : yabai -m window --swap south
        shift + rcmd - i : yabai -m window --swap north
        shift + rcmd - l : yabai -m window --swap east
      '';
    };
    yabai = {
      enable = false;
      package = pkgs.yabai;
      config = {
        auto_balance = "on";
        split_ratio = "0.50";
        layout = "bsp";
      };
      extraConfig = ''
        yabai -m rule --add app='Emacs' manage=on
        yabai -m rule --add app='Amazon Chime' manage=off
        yabai -m rule --add app='System Preferences' manage=off
      '';
    };
  };

  system = {
    keyboard = {
      enableKeyMapping = true;
      remapCapsLockToControl = true;
      swapLeftCommandAndLeftAlt = true;
    };
    defaults = {
      NSGlobalDomain = {
        AppleShowAllFiles = true;
        NSAutomaticWindowAnimationsEnabled = false;
      };
      dock.autohide = true;
    };
    stateVersion = 4;
  };
}
