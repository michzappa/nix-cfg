{ config, inputs, lib, pkgs, ... }:

{
  boot = {
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
    plymouth.enable = true;
  };

  imports = [
    ./hardware.nix
    inputs.nixos-hardware.nixosModules.dell-xps-15-9560-intel
  ];

  mz = {
    android.enable = true;
    bitwarden.enable = true;
    direnv.enable = true;
    docker.enable = true;
    emacs.enable = true;
    firefox.enable = true;
    fish.enable = true;
    gaming.enable = true;
    git.enable = true;
    gnome.enable = true;
    gnupg.enable = true;
    java.enable = true;
    kmonad = {
      enable = true;
      config = builtins.readFile ../../assets/keyboard.kbd;
      device = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
      name = "internal";
    };
    mullvad.enable = true;
    pipewire.enable = true;
    proton.enable = true;
    reasoning.enable = true;
    syncthing.enable = true;
  };

  networking = {
    hostName = "ordinateur";
    interfaces.wlp59s0.useDHCP = true;
  };

  services = {
    fwupd.enable = true;
    fstrim.enable = true;
  };

  system.stateVersion = "22.05";

  time.timeZone = "America/New_York";
}
