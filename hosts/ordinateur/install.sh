#! /usr/bin/env sh

# derived from the UEFI manual install instructions circa 21.11

# exit when any command fails
set -e

# partitioning
parted /dev/nvme0n1 -- mklabel gpt
parted /dev/nvme0n1 -- mkpart primary 512MiB -16GiB
parted /dev/nvme0n1 -- mkpart primary linux-swap -16GiB 100%
parted /dev/nvme0n1 -- mkpart ESP fat32 1MiB 512MiB
parted /dev/nvme0n1 -- set 3 esp on

# formatting
mkfs.ext4 -L root /dev/nvme0n1p1
mkswap -L swap /dev/nvme0n1p2
mkfs.fat -F 32 -n boot /dev/nvme0n1p3

# installing
mount /dev/disk/by-label/root /mnt
mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot
swapon /dev/nvme0n1p2

## actually install (will need flakes somehow)
# git clone https://gitlab.com/michzappa/nixcfg /mnt/nixos-config
# nixos-install --flake /mnt/nixos-config#ordinateur
