{ config, lib, modulesPath, pkgs, ... }:

{

  boot = {
    extraModulePackages = [ ];
    initrd = {
      availableKernelModules =
        [ "ata_piix" "uhci_hcd" "virtio_pci" "sr_mod" "virtio_blk" ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" ];
    kernelParams = [ "console=tty1" "console=ttyS0,115200" ];
  };

  fileSystems."/" = {
    device = "/dev/vda1";
    fsType = "ext4";
  };

  imports = [ (modulesPath + "/virtualisation/qemu-vm.nix") ];

  mz = {
    emacs.enable = true;
    fish.enable = true;
    git.enable = true;
    gnome.enable = true;
    user = {
      name = "vm-user";
      password = "";
    };
  };

  system.stateVersion = "22.05";

  time.timeZone = "America/New_York";

  users.mutableUsers = false;

  virtualisation = {
    cores = 6;
    memorySize = 4096;
  };

}
