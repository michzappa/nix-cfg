{ config, inputs, lib, options, pkgs, platform, ... }:

{
  home = {
    homeDirectory = "/home/mzappa";
    stateVersion = "22.11";
    username = "mzappa";
  };

  mz = {
    emacs.enable = true;
    fish.enable = true;
    system.name = "amz-dev";
    user.name = "mzappa";
  };

  programs = {
    fish = {
      functions = {
        "2fa-setup" = "kinit && mwinit -o -s";
        sam = "brazil-build-tool-exec sam $argv";
        "ssh-proxycommand" = "ssh -o ProxyCommand=none $argv[1]";
        register_with_aaa =
          "/apollo/env/AAAWorkspaceSupport/bin/register_with_aaa.py -a $argv[1]";
      };
      interactiveShellInit = with pkgs; ''
        fish_add_path /home/mzappa/.toolbox/bin
      '';
    };
    home-manager.enable = true;
  };
}
