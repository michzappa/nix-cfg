{ config, inputs, lib, pkgs, ... }:

{
  boot = {
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
    plymouth.enable = true;
  };

  imports = [
    ./hardware.nix
    inputs.nixos-hardware.nixosModules.framework-12th-gen-intel
  ];

  mz = {
    direnv.enable = true;
    emacs.enable = true;
    firefox.enable = true;
    fish.enable = true;
    gaming.enable = true;
    git.enable = true;
    gnome.enable = true;
    gnupg.enable = true;
    java.enable = true;
    kmonad = {
      enable = true;
      config = builtins.readFile ../../assets/keyboard.kbd;
      device = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
      name = "internal";
    };
    mullvad.enable = true;
    pipewire.enable = true;
    proton.enable = true;
    reasoning.enable = true;
    syncthing.enable = true;
  };

  networking = {
    hostName = "rorohiko";
    interfaces.wlp166s0.useDHCP = true;
  };

  services.fwupd.enable = true;

  system.stateVersion = "22.11";

  time.timeZone = "America/Chicago";
}
