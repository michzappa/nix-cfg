{ inputs, lib, platform, ... }:

with lib;
with inputs;
let
  configurationMaker = if platform == "darwin" then
    darwin.lib.darwinSystem
  else if platform == "home" then
    home-manager.lib.homeManagerConfiguration
  else
    nixpkgs.lib.nixosSystem;
in rec {
  makeConfiguration = name: system:
    configurationMaker ({
      modules =
        # common modules
        [ ./hosts/${name} ./modules ] ++
        # platform-specific modules
        (if platform == "darwin" then
          [ home-manager.darwinModule ]
        else if platform == "home" then
          [ ]
        else [
          home-manager.nixosModule
          kmonad.nixosModules.default
        ]);
    } //
      # home-manager expects different arguments than darwin and nixos
      (if platform == "home" then {
        extraSpecialArgs = {
          inputs = inputs;
          lib = lib;
          platform = platform;
        };
        pkgs = nixpkgs.legacyPackages.${system};
      } else {
        specialArgs = {
          inputs = inputs;
          lib = lib;
          platform = platform;
        };
        system = system;
      }));

  darwinOptions = conf: (platformSpecificOptions [ "darwin" ] conf);

  homeOptions = conf: (platformSpecificOptions [ "home" ] conf);

  nixosOptions = conf: (platformSpecificOptions [ "nixos" ] conf);

  systemOptions = conf: (platformSpecificOptions [ "darwin" "nixos" ] conf);

  platformSpecificOptions = optionPlatforms: conf:
    (optionalAttrs
      (foldr (optionPlatform: acc: (platform == optionPlatform) || acc) false
        optionPlatforms) conf);
}
